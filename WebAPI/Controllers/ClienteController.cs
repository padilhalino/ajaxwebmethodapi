﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
  public class ClienteController : ApiController
  {
    Cliente[] clientes = new Cliente[] {
      new Cliente() { ClienteId = 1, Nome = "João" },
      new Cliente() { ClienteId = 2, Nome = "José" },
      new Cliente() { ClienteId = 3, Nome = "Jurandir" },
      new Cliente() { ClienteId = 4, Nome = "Jerônimo" },
      new Cliente() { ClienteId = 5, Nome = "Jessé" },
    };

    // /api/Cliente
    public IEnumerable<Cliente> GetClientes()
    {
      return clientes;
    }

    // /api/Cliente/2
    public IHttpActionResult GetCliente(int id)
    {
      var c = clientes.FirstOrDefault(x => x.ClienteId == id);
      if (c == null)
      {
        return NotFound();
      }
      return Ok(c);
    }

    // /api/Cliente?nome=jo
    public IEnumerable<Cliente> GetClientesPorNome(string nome)
    {
      return clientes.Where(x => x.Nome.ToUpper().Contains(nome.ToUpper())).ToList();
    }
  }
}
