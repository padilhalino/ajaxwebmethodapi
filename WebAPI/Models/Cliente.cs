﻿namespace WebAPI.Models
{
  public class Cliente
  {
    public int ClienteId { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Endereco { get; set; }
    public byte[] Imagem { get; set; }
    public string ImagemTipo { get; set; }
  }
}