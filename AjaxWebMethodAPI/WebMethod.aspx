﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebMethod.aspx.cs" Inherits="AjaxWebMethodAPI.WebMethod" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
  <script src="Scripts/jquery-3.4.1.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function () {
      $("#btnInserir").click(function () {
        var nome = $("#txtNome").val();
        var email = $("#txtEmail").val();
        console.log(nome);
        console.log(email);
        $.ajax({
          type: "POST",
          contentType: "application/json; charset=utf-8",
          url: "WebMethod.aspx/InserirCliente",
          data: "{'nome':'" + nome + "','email':'" + email + "'}",
          async: false,
          success: function (response) {
            console.log(response);
            $("#txtNome").val("");
            $("#txtEmail").val("");
            alert("Registro inserido com sucesso!");
          },
          error: function (response) {
            console.log(response);
            alert("Error");
          }
          //error: function () {
          //  alert("Error");
          //}
        });
      });
    });
  </script>

</head>
<body>
  <form id="form1" runat="server">
    <div>
      <h2>Acessar WebMethod</h2>
      <hr />
      <table>
        <tr>
          <td>Nome</td>
          <td><asp:TextBox ID="txtNome" runat="server" ClientIDMode="Static"></asp:TextBox></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static"></asp:TextBox></td>
        </tr>
        <tr>
          <td colspan="2"><input type="button" id="btnInserir" value="Incluir Cliente" /></td>
        </tr>
      </table>

      <%--InserirCliente--%>

    </div>
  </form>
</body>
</html>
