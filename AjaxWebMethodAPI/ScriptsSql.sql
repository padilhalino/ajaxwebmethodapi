﻿--USE [Northwind]
--GO

/****** Object:  Table [dbo].[Clientes]    Script Date: 19/06/2019 09:25:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Clientes](
	[ClienteId] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[Endereco] [nvarchar](150) NULL,
	[Imagem] [varbinary](max) NULL,
	[ImagemTipo] [nvarchar](50) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[ClienteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO



CREATE PROCEDURE spClientesAdd 
	@Nome nvarchar(100), 
	@Email nvarchar(300)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO Clientes (Nome, Email) VALUES (@Nome, @Email)
END