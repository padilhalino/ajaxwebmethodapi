﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

namespace AjaxWebMethodAPI
{
  public partial class WebMethod : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
    [WebMethod]
    public static string InserirCliente(string nome, string email)
    {
      SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conexaoNorthwind"].ConnectionString);
      try
      {
        SqlCommand cmd = new SqlCommand();
        conn.Open();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "spClientesAdd";
        cmd.Parameters.AddWithValue("@Nome", nome);
        cmd.Parameters.AddWithValue("@Email", email);
        cmd.Connection = conn;
        cmd.ExecuteNonQuery();
        conn.Close();
        return "OK";
      }
      catch (Exception ex)
      {
        return ex.Message;
      }
    }
  }
}